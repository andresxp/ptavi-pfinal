#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import simplertp
import secrets
from lxml import etree
import socket
import json
import time
from datetime import datetime
import hashlib


def Escribir_log(Fichero_log, mensaje, modo, valor):
    Fecha = time.time() + 3600
    Fecha_YMD = (
        time.strftime('%Y-%m-%d ', time.gmtime(Fecha)))
    Fecha_Hora = (
        time.strftime("%H", time.gmtime(Fecha)))
    Fecha_Min = (
        time.strftime("%M", time.gmtime(Fecha)))
    Fecha_seg = (
        time.strftime("%S", time.gmtime(Fecha)))
    Tiempo_dia_s = int(Fecha_Hora) * 3600
    Tiempo_dia_s = int(Fecha_Min) * 60 + Tiempo_dia_s
    Tiempo_dia_s = int(Fecha_seg) + Tiempo_dia_s
    SMS_Tiempo = Fecha_YMD + str(Tiempo_dia_s) + " "
    if modo == "enviar":
        fich = open(Fichero_log, 'a')
        SMS_enviar = "Sent to " + valor + ": "
        fich.write(SMS_Tiempo + SMS_enviar + mensaje + "\r\n")
    elif modo == "recibir":
        fich = open(Fichero_log, 'a')
        SMS_recibir = "Received from " + valor + ": "
        fich.write(SMS_Tiempo + SMS_recibir + mensaje + "\r\n")
    elif modo == "acabar":
        fich = open(Fichero_log, 'a')
        fich.write(SMS_Tiempo + mensaje + "\r\n")
    elif modo == "error":
        fich = open(Fichero_log, 'a')
        fich.write(SMS_Tiempo + mensaje + "\r\n")
    elif modo == "empezar":
        try:
            fich = open(Fichero_log, 'r')
            texto_documento = fich.read()
            if texto_documento == "":
                fich.close()
                fich = open(Fichero_log, 'a')
                fich.write(SMS_Tiempo + mensaje + "\r\n")
        except FileNotFoundError:
            fich = open(Fichero_log, 'a')
            fich.write(SMS_Tiempo + mensaje + "\r\n")
    fich.close()


class EchoHandler(socketserver.DatagramRequestHandler):

    Dicc_regis = {}

    def register_json(self, Name_usuario, IP_usuario, Puerto_uaserver,
                      fecha_registro, Expires_usuario):
        """Devuelve un archivo json con todos los registros de clientes."""
        EchoHandler.Dicc_regis[Name_usuario] = []
        EchoHandler.Dicc_regis[Name_usuario].append({
            'address': IP_usuario, 'Puerto': Puerto_uaserver,
            'fecha registro': str(fecha_registro),
            'expires': Expires_usuario})
        with open('registered.json', 'w') as file:
            json.dump(EchoHandler.Dicc_regis, file, indent=4)

    def SendReceive_SMS(self, Mensaje_SIP,  IP, PUERTO):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            my_socket.connect((IP, int(PUERTO)))
            my_socket.send(bytes(Mensaje_SIP, 'utf-8'))
            Mensaje_log = Mensaje_SIP.replace("\r\n", " ")
            Escribir_log(Fichero_log, Mensaje_log, "enviar", IP + ":" + PUERTO)
            data = my_socket.recv(1024)
            Mensaje_log = data.decode('utf-8').replace("\r\n", " ")
            Escribir_log(
                Fichero_log, Mensaje_log, "recibir", IP + ":" + PUERTO)
            print(data.decode('utf-8'))
            self.wfile.write(data)
        return data.decode('utf-8')

    def handle(self):
        data = self.request[0].decode('utf-8')
        print(data)
        Primera_line = True
        Reg = False
        Receptor_encontrado = False
        fecha_registro = time.time() + 3600
        fecha_hora = (
            time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(fecha_registro)))
        IP_usuario = self.client_address[0]
        Puerto_user = str(self.client_address[1])

        Mensaje_log = data.replace("\r\n", " ")
        Escribir_log(
            Fichero_log, Mensaje_log, "recibir",
            IP_usuario + ":" + Puerto_user)

        for line in self.rfile:
            line_deco = line.decode('utf-8')
            if Primera_line:
                x = line_deco.find(" ")
                y = line_deco.find("sip:")
                z = line_deco.find("SIP/2.0")
                MetodoSIP = line_deco[:x]
                Name_receptor = line_deco[y+4:z-1]

                info_receptor = EchoHandler.Dicc_regis.get(Name_receptor)

                Primera_line = False
                if MetodoSIP == "REGISTER":
                    Info_usuario = line_deco[y+4:z-1]
                    Find_dospuntos = Info_usuario.find(":")
                    Name_usuario = Info_usuario[:Find_dospuntos]
                    Puerto_uaserver = Info_usuario[Find_dospuntos+1:]
                    info_emisor = EchoHandler.Dicc_regis.get(Name_usuario)
                    if data.find("Authorization: Digest response=") != -1:
                        Find_expi = data.find("Expires:")
                        Find_auto = data.find("Authorization:")
                        Find_comillas = data.find(chr(34))
                        Expires_user = data[Find_expi+9:Find_auto-2]
                        Response = data[Find_comillas+1:-5]
                        Passwd = Dicc_passwd[Name_usuario][0]["passwd"]
                        dk = hashlib.pbkdf2_hmac(
                            'sha256', bytes(Passwd, 'utf-8'),
                            bytes(Nonce, 'utf-8'), 100000)
                        dk_hex = dk.hex()
                        if dk_hex == Response:
                            EchoHandler.register_json(
                                self, Name_usuario, IP_usuario,
                                Puerto_uaserver,
                                fecha_registro, Expires_user)
                            Mensaje_SIP = "SIP/2.0 200 OK\r\n\r\n"
                            self.wfile.write(bytes(Mensaje_SIP, 'utf-8'))
                            Mensaje_log = Mensaje_SIP.replace("\r\n", " ")
                            Escribir_log(
                                Fichero_log, Mensaje_log, "enviar",
                                IP_usuario + ":" + Puerto_user)
                            Reg = True

                elif MetodoSIP == "INVITE":
                    if info_receptor is not None:
                        Receptor_encontrado = True
                        Mensaje_SIP = line_deco
                        Receptor_IP = info_receptor[0]["address"]
                        Receptor_puerto = info_receptor[0]["Puerto"]
                    else:
                        Mensaje_SIP = "SIP/2.0 404 User Not Found\r\n\r\n"
                        self.wfile.write(bytes(Mensaje_SIP, 'utf-8'))

                        Mensaje_log = Mensaje_SIP.replace("\r\n", " ")
                        Escribir_log(
                            Fichero_log, Mensaje_log, "enviar",
                            IP_usuario + ":" + Puerto_user)

                elif MetodoSIP == "ACK":
                    Mensaje_SIP = line_deco + "\r\n"
                    Receptor_IP = info_receptor[0]["address"]
                    Receptor_puerto = info_receptor[0]["Puerto"]
                    Newsocke = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    with Newsocke as my_socket:
                        my_socket.setsockopt(
                            socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                        my_socket.connect((Receptor_IP, int(Receptor_puerto)))
                        my_socket.send(bytes(Mensaje_SIP, 'utf-8'))
                    Mensaje_log = Mensaje_SIP.replace("\r\n", " ")
                    Escribir_log(
                        Fichero_log, Mensaje_log, "enviar",
                        Receptor_IP + ":" + Receptor_puerto)

                elif MetodoSIP == "BYE":
                    if info_receptor is not None:
                        Mensaje_SIP = line_deco + "\r\n"
                        Receptor_IP = info_receptor[0]["address"]
                        Receptor_puerto = info_receptor[0]["Puerto"]
                        Peticion_valida = (
                            MetodoSIP + " sip:" + Name_receptor +
                            " SIP/2.0" + "\r\n\r\n")
                        if Peticion_valida == data:
                            DataRece = EchoHandler.SendReceive_SMS(
                                self, Mensaje_SIP, Receptor_IP,
                                Receptor_puerto)
                            Mensaje_log = DataRece.replace("\r\n", " ")
                            Escribir_log(
                                Fichero_log, Mensaje_log, "enviar",
                                IP_usuario + ":" + Puerto_user)
                        else:
                            Mensaje_SIP = "SIP/2.0 400 Bad Request\r\n\r\n"
                            self.wfile.write(bytes(Mensaje_SIP, 'utf-8'))
                            Mensaje_log = Mensaje_SIP.replace("\r\n", " ")
                            Escribir_log(
                                Fichero_log, Mensaje_log, "enviar",
                                IP_usuario + ":" + Puerto_user)
                    else:
                        Mensaje_SIP = "SIP/2.0 404 User Not Found\r\n\r\n"
                        self.wfile.write(bytes(Mensaje_SIP, 'utf-8'))
                        Mensaje_log = Mensaje_SIP.replace("\r\n", " ")
                        Escribir_log(
                            Fichero_log, Mensaje_log, "enviar",
                            IP_usuario + ":" + Puerto_user)
                else:
                    Mensaje_SIP = "SIP/2.0 405 Method Not Allowed\r\n\r\n"
                    self.wfile.write(bytes(Mensaje_SIP, 'utf-8'))
                    Mensaje_log = Mensaje_SIP.replace("\r\n", " ")
                    Escribir_log(
                        Fichero_log, Mensaje_log, "enviar",
                        IP_usuario + ":" + Puerto_user)
            else:
                if MetodoSIP == "REGISTER" and line_deco != "\r\n" and not Reg:
                    Find_dospuntos = line_deco.find(":")
                    Expires_usuario = line_deco[Find_dospuntos+2:-2]
                    Peticion_valida = (
                        MetodoSIP + " sip:" + Name_usuario + ":" +
                        Puerto_uaserver + " SIP/2.0\r\n" + "Expires: " +
                        Expires_usuario + "\r\n\r\n")
                    if Peticion_valida == data:
                        if info_emisor is not None:
                            if Expires_usuario == "0":
                                New = EchoHandler.Dicc_regis.pop(Name_usuario)
                                with open('registered.json', 'w') as file:
                                    json.dump(
                                        EchoHandler.Dicc_regis, file, indent=4)

                                Mensaje_SIP = "User desregistrado\r\n\r\n"
                                Mensaje_log = Mensaje_SIP.replace("\r\n", " ")
                                Escribir_log(
                                    Fichero_log, Mensaje_log, "acabar",
                                    IP_usuario + ":" + Puerto_user)
                            else:
                                EchoHandler.register_json(
                                    self, Name_usuario, IP_usuario,
                                    Puerto_uaserver,
                                    fecha_registro, Expires_usuario)
                                Mensaje_SIP = "User Actualizado\r\n\r\n"
                                Mensaje_log = Mensaje_SIP.replace("\r\n", " ")
                                Escribir_log(
                                    Fichero_log, Mensaje_log, "acabar",
                                    IP_usuario + ":" + Puerto_user)

                            Mensaje_SIP = "SIP/2.0 200 OK\r\n\r\n"
                            self.wfile.write(bytes(Mensaje_SIP, 'utf-8'))

                        else:
                            Passwd = Dicc_passwd[Name_usuario][0]["passwd"]
                            Mensaje_SIP = (
                                "SIP/2.0 401 Unauthorized\r\n" +
                                "WWW Authenticate: Digest nonce=" +
                                chr(34) + Nonce + chr(34) + "\r\n\r\n")

                            self.wfile.write(bytes(Mensaje_SIP, 'utf-8'))

                    else:
                        Mensaje_SIP = "SIP/2.0 400 Bad Request\r\n\r\n"
                        self.wfile.write(bytes(Mensaje_SIP, 'utf-8'))

                    Mensaje_log = Mensaje_SIP.replace("\r\n", " ")
                    Escribir_log(
                        Fichero_log, Mensaje_log, "enviar",
                        IP_usuario + ":" + Puerto_user)
                elif MetodoSIP == "INVITE" and Receptor_encontrado:
                    Mensaje_SIP = Mensaje_SIP + line_deco
        if MetodoSIP == "INVITE" and Receptor_encontrado:
            """ estamos  suponiendo que existe el usuraio"""
            Find_legth = Mensaje_SIP.find("gth:")
            find_salto = Mensaje_SIP[Find_legth:].find("\r\n")
            Length = Mensaje_SIP[Find_legth+5:Find_legth+find_salto]
            Find_ori = Mensaje_SIP.find("o=")
            Find_esp = Mensaje_SIP[Find_ori:].find(" ")
            Emisor = Mensaje_SIP[Find_ori+2:Find_ori+Find_esp]
            Find_audio = Mensaje_SIP.find("audio")
            Find_RTP = Mensaje_SIP[Find_audio:].find("RTP")
            Puerto_RTP = Mensaje_SIP[Find_audio+6:Find_audio+Find_RTP-1]
            Peticion_valida = (
                MetodoSIP + " sip:" + Name_receptor + " SIP/2.0" + "\r\n" +
                "Content-Type: application/sdp\r\n" + "Content-Length: " +
                Length + "\r\n\r\n" + "v=0\r\n" + "o=" +
                Emisor + " " + "192.168.1.40\r\n" +
                "s=Sesion SO BAD\r\n" + "t=0\r\n" + "m=audio " +
                Puerto_RTP + " RTP\r\n\r\n")
            if Peticion_valida == data:
                """ el mensaje sip tiene dos \r\n"""
                DataRece = EchoHandler.SendReceive_SMS(
                    self, Mensaje_SIP, Receptor_IP, Receptor_puerto)
                Mensaje_log = DataRece.replace("\r\n", " ")
                Escribir_log(
                    Fichero_log, Mensaje_log, "enviar",
                    IP_usuario + ":" + Puerto_user)
            else:
                Mensaje_SIP = "SIP/2.0 400 Bad Request\r\n\r\n"
                self.wfile.write(bytes(Mensaje_SIP, 'utf-8'))
                Mensaje_log = Mensaje_SIP.replace("\r\n", " ")
                Escribir_log(
                    Fichero_log, Mensaje_log, "enviar",
                    IP_usuario + ":" + Puerto_user)


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos\

    if len(sys.argv) != 2:
        sys.exit("Usage: python3 proxy_registrar.py config")

    Dicc_pr = {}
    Dicc_passwd = {}
    contador = 0
    doc = etree.parse(sys.argv[1])
    raiz = doc.getroot()

    while contador != len(raiz):
        for attr, value in raiz[contador].items():
            Dicc_pr[raiz[contador].tag + " " + attr] = value
        contador = contador + 1

    Fichero_passwdpath_mal = Dicc_pr["database passwdpath"]
    Fichero_passwdpath = Fichero_passwdpath_mal.replace("Andres", "Andrés")

    with open(Fichero_passwdpath, 'r') as data_file:
        Dicc_passwd = json.load(data_file)

    BIT_random = hex(secrets.randbits(92))
    Nonce = BIT_random[BIT_random.find("x") + 1:]

    Fichero_log_mal = Dicc_pr["log path"]
    Fichero_log = Fichero_log_mal.replace("Andres", "Andrés")
    Mensaje_Start = "Starting..."
    Escribir_log(Fichero_log, Mensaje_Start, "empezar", "")

    IP_SERVER = Dicc_pr["server ip"]
    PUERTO_SERVER = int(Dicc_pr["server puerto"])
    Name_Server = Dicc_pr["server name"]
    Mensaje_Inicio = (
        "Server " + Name_Server + " listening at port " +
        str(PUERTO_SERVER) + "...")

    print(Mensaje_Inicio)
    serv = socketserver.UDPServer((IP_SERVER, PUERTO_SERVER), EchoHandler)
    serv.serve_forever()
