import socket
import sys
from lxml import etree
import secrets
import random
import simplertp
import time
from datetime import datetime
import hashlib

Dicc_ua = {}
contador = 0


def Escribir_log(Fichero_log, mensaje, modo, valor):
    Fecha = time.time() + 3600
    Fecha_YMD = (
        time.strftime('%Y-%m-%d ', time.gmtime(Fecha)))
    Fecha_Hora = (
        time.strftime("%H", time.gmtime(Fecha)))
    Fecha_Min = (
        time.strftime("%M", time.gmtime(Fecha)))
    Fecha_seg = (
        time.strftime("%S", time.gmtime(Fecha)))
    Tiempo_dia_s = int(Fecha_Hora) * 3600
    Tiempo_dia_s = int(Fecha_Min) * 60 + Tiempo_dia_s
    Tiempo_dia_s = int(Fecha_seg) + Tiempo_dia_s
    SMS_Tiempo = Fecha_YMD + str(Tiempo_dia_s) + " "
    if modo == "enviar":
        fich = open(Fichero_log, 'a')
        SMS_enviar = "Sent to " + valor + ": "
        fich.write(SMS_Tiempo + SMS_enviar + mensaje + "\r\n")
    elif modo == "recibir":
        fich = open(Fichero_log, 'a')
        SMS_recibir = "Received from " + valor + ": "
        fich.write(SMS_Tiempo + SMS_recibir + mensaje + "\r\n")
    elif modo == "acabar":
        fich = open(Fichero_log, 'a')
        fich.write(SMS_Tiempo + mensaje + "\r\n")
    elif modo == "error":
        fich = open(Fichero_log, 'a')
        fich.write(SMS_Tiempo + mensaje + "\r\n")
    elif modo == "empezar":
        try:
            fich = open(Fichero_log, 'r')
            texto_documento = fich.read()
            if texto_documento == "":
                fich.close()
                fich = open(Fichero_log, 'a')
                fich.write(SMS_Tiempo + mensaje + "\r\n")
        except FileNotFoundError:
            fich = open(Fichero_log, 'a')
            fich.write(SMS_Tiempo + mensaje + "\r\n")
    fich.close()


if len(sys.argv) != 4:
    sys.exit("Usage: python3 uaclient.py config method option")
elif sys.argv[1].find(".xml") == -1:
    sys.exit("Usage: python3 uaclient.py config method option")


doc = etree.parse(sys.argv[1])
raiz = doc.getroot()

while contador != len(raiz):
    for attr, value in raiz[contador].items():
        Dicc_ua[raiz[contador].tag + " " + attr] = value
    contador = contador + 1

"""192.168.1.40  6001 del servidor."""

MetodoSIP = sys.argv[2]
Option = sys.argv[3]

Puerto_proxy = int(Dicc_ua["regproxy puerto"])
Emisor = Dicc_ua["account username"]
Puerto_uaserver = Dicc_ua["uaserver puerto"]
IP_proxy = Dicc_ua["regproxy ip"]
Puerto_rtpaudio = Dicc_ua["rtpaudio puerto"]
Passwd = Dicc_ua["account passwd"]
Fichero_audio_mal = raiz[8].text
Fichero_log_mal = raiz[7].text
Fichero_log = Fichero_log_mal.replace("Andres", "Andrés")
Fichero_audio = Fichero_audio_mal.replace("Andres", "Andrés")

Mensaje_Start = "Starting..."
Escribir_log(Fichero_log, Mensaje_Start, "empezar", "")

SMS_ParteInicio = MetodoSIP + " sip:"
SMS_ParteFinal = " SIP/2.0" + "\r\n"

SMS_ParteResgister = (
    SMS_ParteInicio +
    Emisor + ":" + Puerto_uaserver +
    SMS_ParteFinal)

SMS_CuerpoInvite = (
    "v=0\r\n" + "o=" + Emisor + " " + "192.168.1.40\r\n" +
    "s=Sesion SO BAD\r\n" + "t=0\r\n" + "m=audio " +
    Puerto_rtpaudio + " RTP\r\n")
Content_Length = len(SMS_CuerpoInvite)

Mensaje_ACK = "ACK sip:" + Option + SMS_ParteFinal

if MetodoSIP == "REGISTER":
    Mensaje_SIP = SMS_ParteResgister + "Expires: " + Option + "\r\n"
elif MetodoSIP == "INVITE":
    Mensaje_SIP = (
        SMS_ParteInicio + Option + SMS_ParteFinal +
        "Content-Type: application/sdp\r\n" + "Content-Length: " +
        str(Content_Length) + "\r\n\r\n" + SMS_CuerpoInvite)
else:
    Mensaje_SIP = SMS_ParteInicio + Option + SMS_ParteFinal

Mensaje_log = Mensaje_SIP.replace("\r\n", " ")

print(Mensaje_SIP)
# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((IP_proxy, Puerto_proxy))
    my_socket.send(bytes(Mensaje_SIP, 'utf-8') + b'\r\n')
    Escribir_log(
        Fichero_log, Mensaje_log, "enviar", IP_proxy + ":" + str(Puerto_proxy))
    try:
        data = my_socket.recv(1024)
    except ConnectionResetError:
        Men_log_error = (
            "20101018160243 Error: No server listening at 192.168.1.40 " +
            "port 6002")
        Escribir_log(Fichero_log, Men_log_error, "error", "")
        sys.exit(Men_log_error)
    if MetodoSIP == "INVITE":
        Mensaje_log = data.decode('utf-8').replace("\r\n", " ")
        Escribir_log(
            Fichero_log, Mensaje_log, "recibir",
            IP_proxy + ":" + str(Puerto_proxy))
        print("")
        print(data.decode('utf-8'))
        Find_ok = data.decode('utf-8').find("SIP/2.0 200 OK")
        if Find_ok != -1:
            Find_puerto = data.decode('utf-8').find("m=audio ")
            Puerto_rtp = data.decode('utf-8')[Find_puerto+8:-8]
            Mensaje_SIP = Mensaje_ACK
            my_socket.send(bytes(Mensaje_SIP, 'utf-8') + b'\r\n')
            Mensaje_log = Mensaje_SIP.replace("\r\n", " ")
            Escribir_log(
                Fichero_log, Mensaje_log, "enviar",
                IP_proxy + ":" + str(Puerto_proxy))
            BIT = secrets.randbits(1)
            RTP_header = simplertp.RtpHeader()
            ALEAT = random.randint(1, 999999)
            RTP_header.set_header(marker=BIT, ssrc=ALEAT)
            audio = simplertp.RtpPayloadMp3(Fichero_audio)
            simplertp.send_rtp_packet(
                RTP_header, audio, "192.168.1.40", int(Puerto_rtp))
            Mensaje_log = "RTP AUDIO"
            Escribir_log(
                Fichero_log, Mensaje_log, "enviar",
                "192.168.1.40:" + str(Puerto_rtp))
    elif MetodoSIP == "BYE":
        Mensaje_log = data.decode('utf-8').replace("\r\n", " ")
        Escribir_log(
            Fichero_log, Mensaje_log, "recibir",
            IP_proxy + ":" + str(Puerto_proxy))
        print("")
        print(data.decode('utf-8'))
    elif MetodoSIP == "REGISTER":
        print("")
        print(data.decode('utf-8'))
        Mensaje_log = data.decode('utf-8').replace("\r\n", " ")
        Escribir_log(
            Fichero_log, Mensaje_log, "recibir",
            IP_proxy + ":" + str(Puerto_proxy))

        if data.decode('utf-8') != "SIP/2.0 200 OK\r\n\r\n":

            Find_comillas = data.decode('utf-8').find(chr(34))
            Nonce = data.decode('utf-8')[Find_comillas + 1:-5]

            dk = hashlib.pbkdf2_hmac(
                'sha256', bytes(Passwd, 'utf-8'),
                bytes(Nonce, 'utf-8'), 100000)
            dk_hex = dk.hex()

            Mensaje_SIP = Mensaje_SIP[:-2]

            Mensaje_SIP = (
                Mensaje_SIP + "\r\n" +
                "Authorization: Digest response=" + chr(34) + dk_hex +
                chr(34) + "\r\n")

            my_socket.send(bytes(Mensaje_SIP, 'utf-8') + b'\r\n')
            Mensaje_log = Mensaje_SIP.replace("\r\n", " ")
            Escribir_log(
                Fichero_log, Mensaje_log, "enviar",
                IP_proxy + ":" + str(Puerto_proxy))

            data = my_socket.recv(1024)
            print(data.decode('utf-8'))
            Mensaje_log = data.decode('utf-8').replace("\r\n", " ")
            Escribir_log(
                Fichero_log, Mensaje_log, "recibir",
                IP_proxy + ":" + str(Puerto_proxy))
        else:
            Mensaje_fin = "Finishing."
            Escribir_log(Fichero_log, Mensaje_fin, "acabar", "")

    else:
        print("")
        print(data.decode('utf-8'))
        Mensaje_log = data.decode('utf-8').replace("\r\n", " ")
        Escribir_log(
            Fichero_log, Mensaje_log, "recibir",
            IP_proxy + ":" + str(Puerto_proxy))
