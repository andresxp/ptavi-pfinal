import socketserver
import sys
import simplertp
import secrets
from lxml import etree
import socket
import json
import time
from datetime import datetime
import secrets
import random


def Escribir_log(Fichero_log, mensaje, modo, valor):
    Fecha = time.time() + 3600
    Fecha_YMD = (
        time.strftime('%Y-%m-%d ', time.gmtime(Fecha)))
    Fecha_Hora = (
        time.strftime("%H", time.gmtime(Fecha)))
    Fecha_Min = (
        time.strftime("%M", time.gmtime(Fecha)))
    Fecha_seg = (
        time.strftime("%S", time.gmtime(Fecha)))
    Tiempo_dia_s = int(Fecha_Hora) * 3600
    Tiempo_dia_s = int(Fecha_Min) * 60 + Tiempo_dia_s
    Tiempo_dia_s = int(Fecha_seg) + Tiempo_dia_s
    SMS_Tiempo = Fecha_YMD + str(Tiempo_dia_s) + " "
    if modo == "enviar":
        fich = open(Fichero_log, 'a')
        SMS_enviar = "Sent to " + valor + ": "
        fich.write(SMS_Tiempo + SMS_enviar + mensaje + "\r\n")
    elif modo == "recibir":
        fich = open(Fichero_log, 'a')
        SMS_recibir = "Received from " + valor + ": "
        fich.write(SMS_Tiempo + SMS_recibir + mensaje + "\r\n")
    elif modo == "acabar":
        fich = open(Fichero_log, 'a')
        fich.write(SMS_Tiempo + mensaje + "\r\n")
    elif modo == "empezar":
        try:
            fich = open(Fichero_log, 'r')
            texto_documento = fich.read()
            if texto_documento == "":
                fich.close()
                fich = open(Fichero_log, 'a')
                fich.write(SMS_Tiempo + mensaje + "\r\n")
        except FileNotFoundError:
            fich = open(Fichero_log, 'a')
            fich.write(SMS_Tiempo + mensaje + "\r\n")
    fich.close()


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    Dicc_rtp = {}

    def handle(self):

        data = self.request[0].decode('utf-8')
        print(data)
        Mensaje_log = data.replace("\r\n", " ")
        Escribir_log(
            Fichero_log, Mensaje_log, "recibir",
            IP_proxy + ":" + str(Puerto_proxy))
        x = data.find(" ")
        MetodoSIP = data[:x]
        y = data.find("sip:")
        z = data.find("SIP/2.0")
        Name_receptor = data[y+4:z-1]

        if Name_receptor != Emisor:
            Mensaje_SIP = "SIP/2.0 404 User Not Found\r\n"
            self.wfile.write(bytes(Mensaje_SIP, 'utf-8') + b'\r\n')
            Mensaje_log = Mensaje_SIP.replace("\r\n", " ")
            Escribir_log(
                Fichero_log, Mensaje_log, "enviar",
                IP_proxy + ":" + str(Puerto_proxy))
        else:
            if MetodoSIP == "INVITE":
                Find_legth = data.find("gth:")
                find_salto = data[Find_legth:].find("\r\n")
                Length = data[Find_legth+5:Find_legth+find_salto]
                Find_ori = data.find("o=")
                Find_espa = data[Find_ori:].find(" ")
                NameOri = data[Find_ori+2:Find_ori+Find_espa]
                Find_puerto = data.find("m=audio ")
                PuertoRTP_NameOri = data[Find_puerto+8:-8]
                Peticion_valida = (
                    MetodoSIP + " sip:" + Name_receptor + " SIP/2.0" + "\r\n" +
                    "Content-Type: application/sdp\r\n" + "Content-Length: " +
                    Length + "\r\n\r\n" + "v=0\r\n" + "o=" +
                    NameOri + " " + "192.168.1.40\r\n" +
                    "s=Sesion SO BAD\r\n" + "t=0\r\n" + "m=audio " +
                    PuertoRTP_NameOri + " RTP\r\n\r\n")
                if Peticion_valida == data:
                    EchoHandler.Dicc_rtp[NameOri] = int(PuertoRTP_NameOri)
                    SMS_CuerpoSDP = (
                        "v=0\r\n" + "o=" + Emisor + " " + "192.168.1.40\r\n" +
                        "s=Sesion SO BAD\r\n" + "t=0\r\n" + "m=audio " +
                        Puerto_rtpaudio + " RTP\r\n")
                    Content_Length = len(SMS_CuerpoSDP)
                    Mensaje_SIP = (
                        "SIP/2.0 100 Trying\r\n" +
                        "SIP/2.0 180 Ringing\r\n" +
                        "SIP/2.0 200 OK\r\n" +
                        "Content-Type: application/sdp\r\n" +
                        "Content-Length: " +
                        str(Content_Length) + "\r\n\r\n" + SMS_CuerpoSDP)
                else:
                    Mensaje_SIP = "SIP/2.0 400 Bad Request\r\n"

                self.wfile.write(bytes(Mensaje_SIP, 'utf-8') + b'\r\n')
                Mensaje_log = Mensaje_SIP.replace("\r\n", " ")
                Escribir_log(
                    Fichero_log, Mensaje_log, "enviar",
                    IP_proxy + ":" + str(Puerto_proxy))

            elif MetodoSIP == "ACK":
                for key in EchoHandler.Dicc_rtp:
                    PuertoRTP_NameOri = EchoHandler.Dicc_rtp[key]

                BIT = secrets.randbits(1)
                RTP_header = simplertp.RtpHeader()
                ALEAT = random.randint(1, 999999)
                RTP_header.set_header(marker=BIT, ssrc=ALEAT)
                audio = simplertp.RtpPayloadMp3(Fichero_audio)
                simplertp.send_rtp_packet(
                    RTP_header, audio, "192.168.1.40", int(PuertoRTP_NameOri))

                Mensaje_log = "RTP AUDIO"
                Escribir_log(
                    Fichero_log, Mensaje_log, "enviar",
                    "192.168.1.40" + ":" + str(PuertoRTP_NameOri))

            elif MetodoSIP == "BYE":
                Peticion_valida = (
                    MetodoSIP + " sip:" + Name_receptor +
                    " SIP/2.0" + "\r\n\r\n")

                if Peticion_valida == data:
                    Mensaje_SIP = "SIP/2.0 200 OK\r\n"
                    self.wfile.write(bytes(Mensaje_SIP, 'utf-8') + b'\r\n')
                    Mensaje_log = Mensaje_SIP.replace("\r\n", " ")
                    Escribir_log(
                        Fichero_log, Mensaje_log, "enviar",
                        IP_proxy + ":" + str(Puerto_proxy))
                else:
                    Mensaje_SIP = "SIP/2.0 400 Bad Request\r\n"
                    self.wfile.write(bytes(Mensaje_SIP, 'utf-8') + b'\r\n')
                    Mensaje_log = Mensaje_SIP.replace("\r\n", " ")
                    Escribir_log(
                        Fichero_log, Mensaje_log, "enviar",
                        IP_proxy + ":" + str(Puerto_proxy))
            else:
                Mensaje_SIP = "SIP/2.0 405 Method Not Allowed\r\n\r\n"
                self.wfile.write(bytes(Mensaje_SIP, 'utf-8'))
                Mensaje_log = Mensaje_SIP.replace("\r\n", " ")
                Escribir_log(
                    Fichero_log, Mensaje_log, "enviar",
                    IP_proxy + ":" + str(Puerto_proxy))


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    Dicc_ua = {}
    contador = 0

    if len(sys.argv) != 2:
        sys.exit("Usage: python3 uaserver.py config")

    doc = etree.parse(sys.argv[1])
    raiz = doc.getroot()

    while contador != len(raiz):
        for attr, value in raiz[contador].items():
            Dicc_ua[raiz[contador].tag + " " + attr] = value
        contador = contador + 1

    Emisor = Dicc_ua["account username"]
    Puerto_uaserver = Dicc_ua["uaserver puerto"]
    IP_uaserver = Dicc_ua["uaserver ip"]
    IP_proxy = Dicc_ua["regproxy ip"]
    Puerto_proxy = int(Dicc_ua["regproxy puerto"])
    Puerto_rtpaudio = Dicc_ua["rtpaudio puerto"]
    Fichero_audio_mal = raiz[8].text
    Fichero_log_mal = raiz[7].text
    Fichero_log = Fichero_log_mal.replace("Andres", "Andrés")
    Fichero_audio = Fichero_audio_mal.replace("Andres", "Andrés")

    Mensaje_Start = "Starting..."

    Escribir_log(Fichero_log, Mensaje_Start, "empezar", "")

    print("Listening...")
    serv = socketserver.UDPServer(
        (IP_uaserver, int(Puerto_uaserver)), EchoHandler)
    serv.serve_forever()
